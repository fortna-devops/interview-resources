from interview_coding_full_time import find_k


def test_find_k_stardard():
    alist = [0.1, 0.4, 0.8, 1, 0.5, 0.3]
    assert find_k(alist) == 3


def test_find_k_no_one():
    alist = [0.1, 0.533, 0.534, 0.44]
    assert find_k(alist) == 0


def test_find_k_nothing_in_list():
    alist = []
    assert find_k(alist) == 0
