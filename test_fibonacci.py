from interview_coding_full_time import fibonacci


def test_fibonacci():
    assert fibonacci(1) == 0
    assert fibonacci(2) == 1
    assert fibonacci(3) == 1
    assert fibonacci(4) == 2
    assert fibonacci(8) == 13
    assert fibonacci(10) == 34
    assert fibonacci(12) == 89
