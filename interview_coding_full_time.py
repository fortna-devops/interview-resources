import ipdb


'''
Find the Nth number in Fibonacci sequence.

A Fibonacci sequence is defined as follow:

The first two numbers are 0 and 1.
The i th number is the sum of i-1 th number and i-2 th number.
The first ten numbers in Fibonacci sequence is:

0, 1, 1, 2, 3, 5, 8, 13, 21, 34 ...

Example
Given 1, return 0

Given 2, return 1

Given 10, return 34

Notice
The Nth fibonacci number won't exceed the max value of signed 32-bit integer in the test cases.

'''


def fibonacci(n):
    pass


'''
Given a list of bits
{a1, a2, a3} where ai in range {0,1}
find an index k such that ak = 1 or return 0 if none exists

the only operation you have access to is
I(i,j) = 1 if at least one 1 is containd in ai, ai+1, ai+2.., aj-1, aj
I(i,j) = 0 if only 0 is contained in ai, ai+1, ai+2.., aj-1, aj
'''


def find_k(alist):
    pass


def helper(alist, startind, endind):
    # assume this is a constant time operation
    list2check = alist[startind:endind]

    val2return = 1 in list2check
    return int(val2return)


'''
A  shift  of  a  sorted  array  by k,
means  removing  the  last k elements and placing them at the beginning.

For example, if A=[1,2,3,4,5,6,7] and k= 3 then new A = [5,6,7,1,2,3,4].
Given a sorted list A that  has  been  shifted  by  an  unknown k,

'''


def shifted_array(sorted_list):
    pass

#shiftd_array([5,6,7,1,2,3,4]) >>3


'''

the concept of palindrome number is a number that reads the same from left to right
and from right to left

for Example, 12121, 45454 and 6776 are valid palindrome numbers.
'''


def is_palindrome(num):
    pass


'''
There are two sorted arrays A and B of size m and n respectively.
Find the median of the two sorted arrays.

Given A=[1,2,3,4,5,6] and B=[2,3,4,5], the median is 3.5.
Given A=[1,2,3] and B=[4,5], the median is 3.
'''


def find_median(sorted_array1, sorted_array2):
    pass

# find_median([1,2,3],[4,5]) ==> 3


'''
Given a root of Binary Search Tree with unique value for each node.
Remove the node with given value. If there is no such a node with
given value in the binary search tree, do nothing.
You should keep the tree still a binary search tree after removal.
'''

'''
Given binary search tree:
    5
   / \
  3   6
 / \
2   4

Remove 3, you can either return

    5
   / \
  2   6
   \
    4

or

    5
   / \
  4   6
 /
2

'''


def removeNode(root, value):
    pass
