import ipdb


'''
A is a sorted list of n different elements in ascending order.
Consider A = [10,23,36,37,47,59,64,71] for example.
Using python language specific syntax, determine if a given element
is contained in the list, A.

'''

def find_element_py(sorted_list,element):
    pass


#find_element_py([10,23,36,37,47,59,64,71], 36) >>True



'''
A  shift  of  a  sorted  array  by k,
means  removing  the  last k elements and placing them at the beginning.

For example, if A=[1,2,3,4,5,6,7] and k= 3 then new A = [5,6,7,1,2,3,4].
Given a sorted list A that  has  been  shifted  by  an  unknown k,
give  an O(n) algorithm to find k

'''

def shifted_array(sorted_list):
    pass

#shiftd_array([5,6,7,1,2,3,4]) >>3


'''
try to implement this with O(log(n)) algorithm
'''
def shifted_array(sorted_list):
    pass

#shiftd_array([5,6,7,1,2,3,4]) >>3


'''
Find the Nth number in Fibonacci sequence.

A Fibonacci sequence is defined as follow:

The first two numbers are 0 and 1.
The i th number is the sum of i-1 th number and i-2 th number.
The first ten numbers in Fibonacci sequence is:

0, 1, 1, 2, 3, 5, 8, 13, 21, 34 ...

Example
Given 1, return 0

Given 2, return 1

Given 10, return 34

Notice
The Nth fibonacci number won't exceed the max value of signed 32-bit integer in the test cases.

'''


def fibonacci(n):
    pass


'''

Given an input string, reverse the string word by word.

For example,
Given s = "the sky is blue",
return "blue is sky the".

Clarification
What constitutes a word?
A sequence of non-space characters constitutes a word.
Could the input string contain leading or trailing spaces?
Yes. However, your reversed string should not contain leading or trailing spaces.
How about multiple spaces between two words?
Reduce them to a single space in the reversed string.
'''

def reverseString(s):
    s.strip()[::-1]




'''
Reverse a 3-digit integer.

Example
Reverse 123 you will get 321.

Reverse 900 you will get 9.

Notice
You may assume the given number is larger or equal to 100 but smaller than 1000.
'''

def reverseInteger(num):
    digit1 = num/100 %
ipdb.set_trace()

'''

[Bash Programming][Regular Expression]
Given a text file file.txt that contains list of phone numbers (one per line), write a one liner bash script to print all valid phone numbers.

You may assume that a valid phone number must appear in one of the following two formats: (xxx) xxx-xxxx or xxx-xxx-xxxx. (x means a digit)

You may also assume each line in the text file must not contain leading or trailing white spaces.

Example:

Assume that file.txt has the following content:

987-123-4567
123 456 7890
(123) 456-7890
Your script should output the following valid phone numbers:

987-123-4567
(123) 456-7890
'''




































#awk '/^([0-9]{3}-|\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/' file.txt
#sed -n -r '/^([0-9]{3}-|\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/p' file.txt
#grep -P '^(\d{3}-|\(\d{3}\) )\d{3}-\d{4}$' file.txt
