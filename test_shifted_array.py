from interview_coding_full_time import shifted_array


def test_shifted_array_stardard():
    orderedlist = [5, 6, 7, 1, 2, 3, 4]
    assert shifted_array(orderedlist) == 3


def test_shifted_array_no_shift():
    orderedlist = [1, 2, 3, 4]
    assert shifted_array(orderedlist) == 0
