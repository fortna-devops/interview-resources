#!/usr/bin/env python3

"""
You are tasked with building a cache client system. 
The cache client system needs to be able to serve as an interface for developers to store data of all kinds within
a cache. There is a cache service interface which has been provided for you in this file. 
Follow the doc strings at the top of the functions to understand the expected parameters and types.

1. The CacheService is just an interface, you will need to fill out it's actual logic. 

2. Build a cache client that developers can use to effectively interact with the CacheService.


Requirements:

- The cache client should allow developers to be able to store any type of object data within python. 
this could be dictionaries, strings, integers, floats, other python classes etc. 

- The lookup process for retrieving an item should be as efficient as possible. 

- keys must be unique, but values are not guaranteed to be.

- keys stored in the cache must be less than 64 characters, but developers should not be limited in their key sizes when using the cache.

- The CacheService `set` method does not have to have any real implementation for the `cache_expiration` parameter.
However, the CacheClient should allow developers to access this functionality.

Things to consider:

- Developers don't want to be concerned with the low level details of the CacheService.
- Developers don't care about how their data is stored, just that they can reliably retrieve it quickly.
- Potential use case might be that a developer is writing some complex SQL queries that are used very frequently and they want 
to cache the result to improve speed.

"""


class CacheClient():
    """
    Cache Client for making it easier for developers to interact with the CacheService
    TODO: Fill this out with whatever types of functions/method you think would accomplish these goals
    and requirements listed above.
    """

    def __init__(self):
        pass


class CacheService():
    """
    Cache Service Interface.
    Do not edit the method signatures.
    """

    def __init__(self):
        """
        TODO: Fill this out

        """

        pass

    def get(self, cache_key):
        """
        TODO: Fill this out


        Retrieve a value from cache memory using a given key

        :param cache_key: The key corresponding to the value to be retrieved.
        :cache_key type: str

        :returns: the cached data or None if the key doesn't exist
        :rtype: str or byte string

        """
        pass

    def set(self, cache_key, data2cache, cache_expiration=7200):
        """
        TODO: Fill this out, pay close attention to the required data type for data2cache

        Set a key/value pair to be placed inside memcache

        :param cache_key: the key to be placed in the cache
        :cache_key type: str

        :param data2cache: the data or value to be placed in the cache with cache_key
        :data2cache type: str or byte string

        :param cache_expiration: The length of time in seconds object should live. (no implementation required)
        :cache_expiration type: int
        """
        pass
