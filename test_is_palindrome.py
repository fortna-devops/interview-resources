from interview_coding_full_time import is_palindrome


def test_is_palindrome():
    assert is_palindrome(12121) == True
    assert is_palindrome(45454) == True
    assert is_palindrome(6776) == True
    assert is_palindrome(67765) == False
    assert is_palindrome(1) == True
